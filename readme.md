# StudentView

StudentView est une application Java pour afficher une liste d'étudiants.
### Fonctionnalités

Affichage de la liste des étudiants
Filtrage de la liste des étudiants par âge
Ajout d'un nouvel étudiant à la liste (avec un jeu de mot de préférence :') )
Modification d'un étudiant existant dans la liste
Suppression d'un étudiant de la liste

### Technologies utilisées
* Java 11
* Java Swing
* JDBC
* SQL
* Oracle Database

### Installation
1. Téléchargez le code source de l'application depuis le repository GitHub.
2. Importez le projet dans votre IDE Java.
3. Ajoutez la bibliothèque JDBC 11 à votre projet. Vous pouvez la télécharger depuis le site officiel d'Oracle.
4. Compilez le projet en utilisant votre IDE.
5. Lancez l'application en exécutant la classe principale "Main".

### Utilisation
L'application affiche la liste des étudiants par défaut.
Utilisez le champ de recherche pour filtrer les étudiants par âge.
Cliquez sur le bouton "Save new" pour ajouter un nouvel étudiant.
Cliquez sur le bouton "Edit" pour modifier les détails d'un étudiant existant.
Cliquez sur le bouton "delete" pour supprimer un étudiant de la liste.
### Auteur
Mathys Lioson - mathyslioson@gmail.com