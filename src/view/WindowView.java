package view;

import database.ConnectionDB;
import person.Person;

import javax.swing.*;

import static javax.swing.JOptionPane.showMessageDialog;

public class WindowView extends JFrame {
    private ConnectionDB connection;
    private Person currentPerson;
    private JButton btLast;
    private JButton btFist;
    private JButton btPrev;
    private JButton btNext;
    private JTextField txtAge;
    private JTextField txtNum;
    private JTextField txtLastName;
    private JTextField txtFirstName;
    private JPanel panelMain;
    private JButton ALLButton;
    private JButton searchButton;
    private JTextField searchArea;
    private JButton emptyFormBtn;
    private JButton saveNewBtn;
    private JButton editBtn;
    private JButton deleteBtn;
    private JButton btFirst;


    public WindowView(){
        setContentPane(panelMain);
        setSize(750, 500);
        initConnection();
        initButton();
    }

    private void initConnection(){
        connection = new ConnectionDB();
        currentPerson = connection.first();
    }

    private void initButton(){
        btFirst.addActionListener(e -> {
            canceling();
            if(txtNum.isEnabled()){
                currentPerson = connection.first();
                showInfo();
            }
        });

        btLast.addActionListener(e -> {
            canceling();
            if(txtNum.isEnabled()){
                currentPerson = connection.last();
                showInfo();
            }
        });

        btPrev.addActionListener(e -> {
            canceling();
            if (txtNum.isEnabled()) {
                currentPerson = connection.previous();
                ifIsFirstPersonShowTheSame();
                showInfo();
            }
        });


        btNext.addActionListener(e -> {
            canceling();
            if (txtNum.isEnabled()) {
                currentPerson = connection.next();
                ifIsLastPersonShowTheSame();
                showInfo();
            }
        });

        ALLButton.addActionListener(e -> {
            canceling();
            if (txtNum.isEnabled()){
                connection.showAll();
                currentPerson = connection.first();
                showInfo();
            }
        });

        searchButton.addActionListener(e -> {
            canceling();
            if (txtNum.isEnabled()){
                currentPerson = connection.ageSearch(searchArea.getText());
                showInfo();
            }
        });

        emptyFormBtn.addActionListener(e -> {
            // set editbutton to false deletebutton to false
            editBtn.setEnabled(false);
            deleteBtn.setEnabled(false);

            txtNum.setText(connection.nextNum());
            txtFirstName.setText("");
            txtLastName.setText("");
            txtAge.setText("");
            txtNum.setEnabled(false);
        });

        saveNewBtn.addActionListener(e -> {
            if (txtLastName.getText().equals("") || txtFirstName.getText().equals("")) {
                showMessageDialog(null, "please complete all fields");
            } else if (txtLastName.getText().matches(".*\\d.*") || txtFirstName.getText().matches(".*\\d.*")) {
                showMessageDialog(null, "Name and Firstname cannot contain numbers");
            }else if (txtLastName.getText().matches(".*\\s.*") || txtFirstName.getText().matches(".*\\s.*")) {
                showMessageDialog(null, "Name and Firstname cannot contain spaces");
            } else if (!txtAge.getText().equals("")){
                if (Integer.parseInt(txtAge.getText()) < 0) {
                    showMessageDialog(null, "Age cannot be negative");
                }
            }
            if (txtAge.getText().equals(""))
                txtAge.setText("-1");

            connection.saveNew(txtNum.getText(), txtLastName.getText(), txtFirstName.getText(), txtAge.getText());
            currentPerson = connection.first();
            showInfo();

            // set editbutton to true deletebutton to true
            editBtn.setEnabled(true);
            deleteBtn.setEnabled(true);
            txtNum.setEnabled(true);
        });

        editBtn.addActionListener(e -> {
            connection.edit(txtNum.getText(), txtLastName.getText(), txtFirstName.getText(), txtAge.getText());
            currentPerson = connection.first();
            showInfo();
        });

        deleteBtn.addActionListener(e -> {
            // dialog box do you want to delete this person
            int reply = JOptionPane.showConfirmDialog(null, "Do you want to delete this person?", "Delete", JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_OPTION) {
                // We delete the person who has been created but not saved
                connection.delete(txtNum.getText());
                currentPerson = connection.first();
                showInfo();
            }
            if (reply == JOptionPane.NO_OPTION) {
                // We do nothing
            }
        });


    }

    private void canceling() {
        if (!txtNum.isEnabled()) {
            cancelCreation();
        }
    }

    private void cancelCreation() {
        int reply = JOptionPane.showConfirmDialog(null, "Do you want to cancel the creation of this person?", "Cancel", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            // We delete the person who has been created but not saved
            connection.delete(txtNum.getText());
            txtNum.setEnabled(true);
            currentPerson = connection.first();
            showInfo();

            // set editbutton to true deletebutton to true
            editBtn.setEnabled(true);
            deleteBtn.setEnabled(true);
            }
        if (reply == JOptionPane.NO_OPTION) {
            // We do nothing
        }
    }

    private void ifIsFirstPersonShowTheSame() {
        if (currentPerson == null)
            currentPerson = connection.first();
    }

    private void showInfo() {
        txtNum.setText(String.valueOf(currentPerson.getNum()));
        txtFirstName.setText(currentPerson.getFirstName());
        txtLastName.setText(currentPerson.getLastName());
        txtAge.setText(getAge());
    }

    private String getAge() {
        if (currentPerson.getAge() == -1)
            return "inco";
        else
            return String.valueOf(currentPerson.getAge());
    }

    private void ifIsLastPersonShowTheSame() {
        if (currentPerson == null)
            currentPerson = connection.last();
    }

}
