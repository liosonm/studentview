package database;

import person.Person;

import javax.swing.*;
import java.sql.*;

import static javax.swing.JOptionPane.showMessageDialog;

public class ConnectionDB {
    private Statement state;
    private PreparedStatement prepState;
    private ResultSet rs;
    private Connection cn;
    private String query;
    private String num;
    private String lastName;
    private String firstName;
    private int age;
    private int maxAge;


    public ConnectionDB() {
        // initialisation of the connection to the database
        try {
            cn = DriverManager.getConnection("jdbc:oracle:thin:@162.38.222.149:1521:iut", "liosonm", "13062002");
            state = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        setDefaultQuery();
    }




    public Person first() {
        setDefaultQuery();// Execution of the SQL query to get the first person
        try {
            rs = state.executeQuery(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            rs.first();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            num = rs.getString("CODEPERSONNE");
            lastName = rs.getString("NOMPERSONNE");
            firstName = rs.getString("PRENOMPERSONNE");
            age = getAgepersonne();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Creation of a Person object with the retrieved data
        return new Person(num, lastName, firstName, age);
    }




    public Person last() {
        setDefaultQuery();
        // Execution of the SQL query to get the last person
        try {
            rs.last();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            num = rs.getString("CODEPERSONNE");
            lastName = rs.getString("NOMPERSONNE");
            firstName = rs.getString("PRENOMPERSONNE");
            age = getAgepersonne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Creation of a Person object with the retrieved data
        return new Person(num, lastName, firstName, age);
    }

    public Person next() {
        // Execution of the SQL query to get the next person
        try {
            if(rs.isLast()){
                return null;
            }
            rs.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            num = rs.getString("CODEPERSONNE");
            lastName = rs.getString("NOMPERSONNE");
            firstName = rs.getString("PRENOMPERSONNE");
            age = getAgepersonne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Creation of a Person object with the retrieved data
        return new Person(num, lastName, firstName, age);
    }

    public Person previous() {
        // Execution of the SQL query to get the previous person
        try {
            if(rs.isFirst()){
                return null;
            }
            rs.previous();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            num = rs.getString("CODEPERSONNE");
            lastName = rs.getString("NOMPERSONNE");
            firstName = rs.getString("PRENOMPERSONNE");
            age = getAgepersonne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Creation of a Person object with the retrieved data
        return new Person(num, lastName, firstName, age);
    }

    public void disconnect() {
        // Disconnection from the database
        try {
            cn.close();
            state.close();
            rs.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private int getAgepersonne() throws SQLException {
        int age = rs.getInt("AGEPERSONNE");
        if (rs.wasNull()) {
            return -1;
        }
        return age;
    }

    public void showAll() {
        query = "SELECT * FROM PERSONNES ORDER BY AGEPERSONNE DESC NULLS LAST";
    }

    public Person ageSearch(String text) {
        // people with a supperior or equals age to the one entered if the date entered is nan it will show an error message with prepareStatement

        checkText(text);
        query = "SELECT * FROM PERSONNES WHERE AGEPERSONNE >= ? ORDER BY AGEPERSONNE DESC NULLS LAST";
        try {
            prepState = cn.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prepState.setInt(1, Integer.parseInt(text));
            rs = prepState.executeQuery();
            rs.first();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            num = rs.getString("CODEPERSONNE");
            lastName = rs.getString("NOMPERSONNE");
            firstName = rs.getString("PRENOMPERSONNE");
            age = getAgepersonne();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Creation of a Person object with the retrieved data
        return new Person(num, lastName, firstName, age);
    }

    private void checkText(String text) {
        notANumber(text);

        if (text.equals("")) {
            // message dialog if the text field is empty
            showMessageDialog(null, "Please enter an age", "Error", JOptionPane.ERROR_MESSAGE);
        }
        if (Integer.parseInt(text) > 150) {
            showMessageDialog(null, "Please enter an age below 150", "Error", JOptionPane.ERROR_MESSAGE);
        }
        if (Integer.parseInt(text) > maxAge) {
            showMessageDialog(null, "Please enter an age below " + maxAge, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private static void notANumber(String text) {
        try {
            Integer.parseInt(text);
        } catch (NumberFormatException e) {
            showMessageDialog(null, "Please enter a number", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void getMaxAge() {
        // Execution of the SQL query to get the max age
        try {
            rs = state.executeQuery("SELECT MAX(AGEPERSONNE) FROM PERSONNES");
            rs.first();
            maxAge = rs.getInt(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void setDefaultQuery() {
        getMaxAge();
        // default query to get all the people
        query ="SELECT * FROM PERSONNES ORDER BY AGEPERSONNE DESC NULLS LAST";
        // default query to get all the people
        try {
            rs = state.executeQuery(query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void saveNew(String num, String lastName, String firstName, String age) {
        // save a new person into the database using where the user entered in the text fields
        String insertquery = "INSERT INTO PERSONNES (CODEPERSONNE, NOMPERSONNE, PRENOMPERSONNE, AGEPERSONNE) VALUES (?, ?, ?, ?)";
        try {
            prepState = cn.prepareStatement(insertquery);
            prepState.setString(1, num);
            prepState.setString(2, lastName);
            prepState.setString(3, firstName);
            prepState.setString(4, age);
            prepState.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void edit(String num, String lastName, String firstName, String age) {

        if (age.equals("inco")) {
            age = "-1";
        }
        // edit a person in the database
        String updatequery = "UPDATE PERSONNES SET NOMPERSONNE = ?, PRENOMPERSONNE = ?, AGEPERSONNE = ? WHERE CODEPERSONNE = ?";
        try {
            prepState = cn.prepareStatement(updatequery);
            prepState.setString(1, lastName);
            prepState.setString(2, firstName);
            prepState.setString(3, age);
            prepState.setString(4, num);
            prepState.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public String nextNum() {
        String nextNumQuery = "SELECT seq_codePersonne.nextval FROM dual";
        try {
            rs = state.executeQuery(nextNumQuery);
            rs.first();
            return rs.getString(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public void delete(String num) {
        // delete a person in the database
        String deletequery = "DELETE FROM PERSONNES WHERE CODEPERSONNE = ?";
        try {
            prepState = cn.prepareStatement(deletequery);
            prepState.setString(1, num);
            prepState.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
