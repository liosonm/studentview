package person;

public class Person {
    private String num;
    private String lastName;
    private String firstName;
    private int age;

    public Person(String num, String lastName, String firstName, int age) {
        this.num = num;
        this.lastName = lastName;
        this.firstName = firstName;
        this.age = age;
    }

    public String getNum() {
        return num;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getAge() {
        return age;
    }
}